<?php

App::uses('AppShell', 'Console/Command');
App::uses('HttpSocket', 'Network/Http');

class RestTask extends AppShell {

	protected function sendRequest($method, $api) {
		if(!in_array($method, array('get', 'post', 'put', 'delete'))) {
			throw new CakeException('Unsupported rest api method: '.$method);
		}

		if(isset($this->params['user'])) {
			$user = $this->params['user'];
			unset($this->params['user']);
		} else {
			$user = Configure::read('bitbucket.username');
		}

		if(isset($this->params['pass'])) {
			$pass = $this->params['pass'];
			unset($this->params['pass']);
		} else {
			$pass = Configure::read('bitbucket.password');
		}

		$http = new HttpSocket();
		$http->configAuth('Basic', $user, $pass);

		$api = 'https://bitbucket.org/api/1.0/'.$api;

		$resp = $http->$method($api, $this->params);
		$json = json_decode($resp['body'], true);

		if(!is_array($json)) {
			debug($resp);
			throw new CakeException('Invalid response from bitbucket:');
		}

		return $json;
	}

	public function getOptionParser() {
		$parser = parent::getOptionParser();
		return $parser->addOption('user', array(
			'help' => __d('bitbucket_console', 'Bitbucket username'),
			'short' => 'u',
		))->addOption('pass', array(
			'help' => __d('bitbucket_console', 'Bitbucket password'),
			'short' => 'p',
		));
	}
}
