<?php

class RestShell extends AppShell {

	public $tasks = array('Bitbucket.Repository');

	public function getOptionParser() {
		$parser = parent::getOptionParser();
		return $parser->description(
			__d('bitbucket_console', 'Bitbucket Rest API Console'
		))->addSubCommand('repository', array(
			'help' => __d('bitbucket_console', 'Respository management'),
			'parser' => $this->Repository->getOptionParser(),
		));
	}
}
