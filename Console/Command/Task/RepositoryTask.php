<?php

App::uses('RestTask', 'Bitbucket.Console/Command/Task');

class RepositoryTask extends RestTask {

	public function execute() {
		$action = $this->args[0];
		$this->$action();
	}

	public function create() {
		$name = trim(@$this->params['name']);
		if(empty($name)) throw new CakeException('name is mandatory for create action');

		$resp = $this->sendRequest('post', 'repositories');
		debug($resp);
	}

	public function show() {
		$resp = $this->sendRequest('get', 'user/repositories');
		$resp = array_map(function($a) { return array('resource_uri' => @$a['resource_uri'], 'name' => @$a['name']); }, $resp);

		debug($resp);
	}

	public function getOptionParser() {
		$parser = parent::getOptionParser();
		return $parser->description(
			__d('bitbucket_console', 'Bitbucket Repository Management')
		)->addArgument('action', array(
			'help' => __d('bitbucket_console', 'The rest action, create, delete, list, get'),
			'required' => true,
			'choices' => array('create', 'delete', 'show', 'get'),
		))->addOption('name', array(
			'help' => __d('bitbucket_console', 'The name of the repository. It\'s mandatory'),
		))->addOption('description', array(
			'help' => __d('bitbucket_console', 'A description of the repository.'),
		))->addOption('scm', array(
			'help' => __d('bibucket_console', 'A value of git or hg. The default is git if you leave this parameter unspecified.'),
			'default' => 'git',
		))->addOption('language', array(
			'default' => 'php',
			'help' => __d('bibucket_console', 'The language used for source code in the repository. Default is php.'),
		))->addOption('is_private', array(
			'default' => 'true',
			'help' => __d('bibucket_console', 'Boolean specifying if the repository is private (true) or public (false).  The default is true.'),
		));
	}
}
